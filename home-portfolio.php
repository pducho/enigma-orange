<!-- portfolio section -->
<?php $wl_theme_options = weblizar_get_options(); ?>
<div class="enigma_project_section">
<?php if($wl_theme_options['port_heading'] !='') { ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="enigma_heading_title">
					<h3><?php echo esc_attr($wl_theme_options['port_heading']); ?></h3>		
				</div>
			</div>
		</div>
	</div>
<?php } ?>
<div class="container">
<div class="entry-content-page">
	<div class="portfolio-content">
		<h2>Vítáme Vás na našich nových stránkách</h2>
		<p><b>Příprava na zkoušku</b> je výuková agentura s již sedmiletou tradicí. Naší snahou je připravit žáky pátých a sedmých ročníků ke studiu na víceletých gymnáziích a také chlapce &nbsp;a dívky devátých ročníků, kteří skládají přijímací zkoušky na gymnázia a další střední školy. Vycházíme ze současného způsobu testování a cíleně vedeme výuku, aby byli všichni schopni reagovat na zadané úkoly přijímacích zkoušek. Naší vizitkou jsou desítky studentů přijatých na střední školy a víceletá gymnázia.</p>
		<p>Pro úspěšné zvládnutí přijímacích zkoušek na <b>osmiletá, šestiletá, čtyřletá gymnázia a střední školy </b>pro vaše děti nabízíme:</p>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-3 pull-left scrollimation fade-right d1 portfolio-box">
		<h2>PŘÍPRAVNÉ KURZY Z&nbsp;ČESKÉHO JAZYKA A MATEMATIKY</h2>
		<p>Připravujeme žáky výukou v&nbsp;malých skupinách (max. 6 žáků). Zahajujeme v polovině října, tedy v&nbsp;týdnu od 16.10.2017.</p>
		<p>Probíhají <strong>Na&nbsp;Zámecké 1518/9, Praha 4 – Nusle</strong> (budova IKOS, oranžový zvonek PNZ, 2.patro)</p>
		
		<p>Termíny pro žáky 5. tříd</p>
		<p>Termíny pro žáky 7. tříd</p>
		<p>Termíny pro žáky 9. tříd</p>
		<p><b><br>
		Způsob úhrady: </b>platba ve dvou splátkách – 1. splátka 2.000 Kč &nbsp;začátkem kurzu, 2. splátka v&nbsp;lednu 2018. V&nbsp;případě, že nastoupíte do kurzu později, platíte poměrnou část.<b>Cena:</b> &nbsp;4.000 Kč / 20 lekcí (1 lekce = 60 min)</p>
		<p><b>Jak se přihlásit:</b> vyberte termín a kontaktujte nás mailem na <a href="mailto:info@pripravanazkousku.cz">info@pripravanazkousku.cz</a> nebo telefonicky 605&nbsp;262&nbsp;291. My vám termín potvrdíme a zarezervujeme.</p>
	</div>
	
	<div class="col-lg-6 col-md-6 col-sm-3 pull-right scrollimation fade-left d1 portfolio-box">
		<h2>POROVNÁVACÍ ZKOUŠKY NANEČISTO</h2>
		<p>Z <b>matematiky</b> a 
		<b>českého jazyka</b></p>
		<p><b>Probíhá</b> Na Zámecké 1518/9, Praha 4 – Nusle (budova IKOS, oranžový zvonek PNZ, 2.patro)</p>
		<p><b>Kdy:</b> &nbsp;každý týden v&nbsp;<b>sobotu od 10 do 12.30 h</b> v&nbsp;období <b>od 4.11.2017 až do „přijímaček“</b></p>
		<p><b>Cena: &nbsp;</b>300 Kč</p>
	</div>
	<div style="clear: both;"></div>
</div>
<div style="clear: both;"></div>
<?php /*
		<div class="row" >
			<div id="enigma_portfolio_section" class="enima_photo_gallery">
			



				<?php for($i=1 ; $i<=4; $i++) { ?>
				<?php if($wl_theme_options['port_'.$i.'_img'] !='') { ?>
				<div class="col-lg-6 col-md-6 col-sm-6 pull-left scrollimation d1"> 
					<div class="img-wrapper">
					
						<div class="enigma_home_portfolio_showcase">
							<img class="enigma_img_responsive" alt="<?php the_title_attribute(); ?>" src="<?php echo esc_url($wl_theme_options['port_'.$i.'_img']); ?>">
							<div class="enigma_home_portfolio_showcase_overlay">
								<div class="enigma_home_portfolio_showcase_overlay_inner ">
									<div class="enigma_home_portfolio_showcase_icons">
										<a title="<?php echo esc_attr($wl_theme_options['port_'.$i.'_title']); ?>" href="<?php echo esc_url($wl_theme_options['port_'.$i.'_link']); ?>"><i class="fa fa-link" ></i></a>
										<a class="photobox_a"  href="<?php echo esc_url($wl_theme_options['port_'.$i.'_img']); ?>"><i class="fa fa-search-plus"></i><img src="<?php echo esc_url($wl_theme_options['port_'.$i.'_img']); ?>" alt="<?php echo esc_attr($wl_theme_options['port_'.$i.'_title']); ?>" style="display:none !important;visibility:hidden"></a>
									</div>
								</div>
							</div>
						</div>
					
					<?php if($wl_theme_options['port_'.$i.'_title'] !='') { ?>	
					<div class="enigma_home_portfolio_caption">
					<h3><a target="_blank" href="<?php echo esc_url($wl_theme_options['port_'.$i.'_link']); ?>"><?php echo esc_attr($wl_theme_options['port_'.$i.'_title']); ?></a></h3>
					</div>
					<?php } ?>
					</div>
					<div class="enigma_portfolio_shadow"></div>
				</div>
				<?php } ?>
				<?php } ?>
			</div>
			</div>
						
		</div>
	</div>
<!-- /portfolio section -->
