<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
<div class="enigma_blog_full">

		<div class="enigma_blog_post_content">
			<?php the_content( __( 'Read More' , 'enigma' ) ); ?>
		</div>
</div>	
<div class="push-right">
	<hr class="blog-sep header-sep">
</div>
<?php comments_template( '', true ); ?>
<?php
endwhile;
endif; ?>